﻿using System;
using System.Collections.Generic;

namespace Day6
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Dictionary<string, string> carsSofia = new Dictionary<string, string>();

            carsSofia.Add("CB9999OO", "Demetrios");
            carsSofia.Add("CB272555", "Pete");
            carsSofia.Add("CB4567FF", "Georgi");
            carsSofia.Add("CB3377GG", "Mihail");
            carsSofia.Add("CB8928FT", "Mihail");
            carsSofia.Add("CB9998XX", "Demetrios");


            Console.WriteLine("Cars in dictionary:");

            
            foreach (dynamic carToOwnerPair in carsSofia)
            {
                string plate = carToOwnerPair.Key;
                string owner = carToOwnerPair.Value;

                Console.WriteLine("Owner "+ owner+ " | plate: " + plate);
            }


            // Store number of cars of each owner
            Dictionary<string, int> multipleCars = new Dictionary<string, int>();
            multipleCars.Add("dimi", 3);
            foreach (dynamic carToOwenrPair in carsSofia)
            {
                if (multipleCars.ContainsKey(carToOwenrPair.Key))
                {
                    multipleCars[carToOwenrPair.Key]++;
                }
                else
                {
                    multipleCars[carToOwenrPair.Key] = 1;
                }
            }

            foreach(dynamic ownerMultiple in multipleCars)
            {
                if(ownerMultiple.Value > 1)
                {
                    Console.WriteLine("Owner " + ownerMultiple.Key + " owns " + ownerMultiple.Value + " cars.");
                }
            }

            Console.WriteLine("Hello, World!");
        }
    }
}