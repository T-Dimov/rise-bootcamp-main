﻿using System;
using System.Collections.Generic;

namespace Task3
{
    internal class Program
    {

        public static char FirstNonRepeatedCharInString(string input)
        {

            Dictionary<char, int> characterDic = new Dictionary<char, int>();
            int temp = 0;

            foreach(char currChar in input.ToCharArray()) { 
                if(characterDic.ContainsKey(currChar))
                {
                    // Get number of occurences in the string and add one
                    temp = characterDic[currChar];       
                    characterDic[currChar] = temp + 1;
                    continue;
                }
                // Add pair of current element and temp
                characterDic.Add(currChar, temp);
            }


            // If we have unique char -> return its key
            if (characterDic.ContainsValue(0))
            {
                return characterDic.First(element => element.Value == 0).Key;
            }


            int defaultReturn = 1;
            // If we dont have unique character => Return 1
            return (char)defaultReturn;
        }


        public static List<char> AllNonRepeatedCharInString(string input)
        {

            Dictionary<char, int> characterDic = new Dictionary<char, int>();
            int temp = 0;

            foreach (char currChar in input.ToCharArray())
            {
                if (characterDic.ContainsKey(currChar))
                {
                    // Get number of occurences in the string and add one
                    temp = characterDic[currChar];
                    characterDic[currChar] = temp + 1;
                    continue;
                }
                // Add pair of current element and temp
                characterDic.Add(currChar, temp);
            }


            List<char> result = new List<char>();

            // If we have unique char -> add them to the list
            foreach (dynamic el in characterDic){
                if (el.Value == 0)
                {
                    result.Add(el.Key);
                }
            }

            return result;
        }





        static void Main(string[] args)
        {

            string input = "twtbestewsa";


            char output2 = FirstNonRepeatedCharInString(input);

            Console.WriteLine("The first unique element is: " + output2);


            Console.WriteLine("_________________");

            List<char> output =  AllNonRepeatedCharInString(input);


            Console.WriteLine("All the unique elements are: ");
            foreach (char currChar in output)
            {
                Console.WriteLine(currChar);
            }
        }
    }
}