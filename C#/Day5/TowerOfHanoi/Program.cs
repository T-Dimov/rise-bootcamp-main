﻿using System.Runtime.CompilerServices;
using System.Security.Cryptography;

namespace TowerOfHanoi
{
    class Program
    {

        static Stack<int> A = new Stack<int>();
        static Stack<int> B = new Stack<int>();
        static Stack<int> C = new Stack<int>();


        public static void Play(int discs)
        {
            //Push the numbers
            StartGame(discs);

            // Print starting stacks
            PrintStacks();


            // Start moving
            MethodSimulate(discs, A, B, C);


            // If we have the end of the methodSimulate
            Console.WriteLine("Finished");
        }


        // Pushg element on the starting pile
        public static void StartGame(int discs)
        {
            for (int i = discs; i > 0; i--)
            {
                A.Push(i);
            }
        }


        public static void MethodSimulate(int disc, Stack<int> currentStack, Stack<int> destinationStack, Stack<int> helperStack)
        {

            // Base case, if we have a single disc left to move
             if (disc == 1)
            {
                MoveDisc(currentStack, destinationStack);

                PrintStacks();
            }

            else
            { 
                // Call the same function again and make a recursion, move the elements to the helper stack except the largest disc in the current stack
                MethodSimulate(disc - 1, currentStack, helperStack, destinationStack);


                // The single largest disc left (from the disc-1) is moved to the destination and won't be moved ever again
                MoveDisc(currentStack, destinationStack);

                PrintStacks() ;


                // Call the same function again and make a recursion, move the elements to the helper stack except the largest disc in the current stack
                MethodSimulate(disc - 1, helperStack, destinationStack, currentStack);
            }

        }


        //Move a single disc
        public static void MoveDisc(Stack<int> currentStack, Stack<int> destinationStack)
        {
            int disc = currentStack.Pop();

            // Push the removed disc on the destination column
            destinationStack.Push(disc);
        }


        // Get information about the stacks
        public static void PrintStacks()
        {

            // Make the smallest number to be printed on the left side of the console
            string Atower = string.Join(" [ ", A);
            string Btower = string.Join(" [ ", B);
            string Ctower = string.Join(" [ ", C);
            char[] charArrayA = Atower.ToCharArray();
            char[] charArrayB = Btower.ToCharArray();
            char[] charArrayC = Ctower.ToCharArray();
            Array.Reverse(charArrayA);
            Array.Reverse(charArrayB);
            Array.Reverse(charArrayC);

            //Return the order
            Console.WriteLine("Tower A:| " + new string(charArrayA));
            Console.WriteLine("Tower B:| " + new string(charArrayB));
            Console.WriteLine("Tower C:| " + new string(charArrayC));
            Console.WriteLine("        ");
            Console.WriteLine("        ");
        }


        static void Main(string[] args)
        {
            //Select the difficulty
            Play(3);
            //Play(5);
            //Play(6);
            //Play(10);
            //Play(20);

        }


    }

}